
var nav = {
    init: function() {
        nav.open();
        nav.itemHover();
    },

    open: function() {    
        var openTL = new TimelineMax({ paused: true });

        openTL.to('.burger', .25, { autoAlpha: 0, ease: Power2.easeInOut }, 0)
                .to('header', .3,{ opacity: 1, width: 300, ease: Power2.easeInOut }, 0)
                .to('.menu__logo', .25, { x: '-50%', left: '40%', ease: Power2.easeInOut }, 0)
                .to('.menu__logo svg #letters', .25, { autoAlpha: 1, ease: Power2.easeInOut }, 0)
                .staggerTo('.menu__nav li', .2, { x: 0, display: 'block', autoAlpha: 1, ease: Power2.easeInOut }, .1)
                .to('.menuOverlay', .5, { display: 'block', autoAlpha: .95, ease: Power2.easeInOut }, 0)
                .to('.website', .5, { autoAlpha: 1, ease: Power2.easeInOut }, 0);

        $('.burger').on('click', function() {
            openTL.play().timeScale(1);
        });

        $('.menuOverlay').on('click', function() {
            openTL.reverse().timeScale(1.5); 
        });
    },

    itemHover: function() {
        $('.menu__nav li a').on('mouseover', function() {
            var line = $(this).next('span');
            var lW =$(this).closest('ul').width() - $(this).width() - 5; 

            TweenMax.to(line, .25,{ width: lW, ease: Power2.easeInOut }, 0);
        });

         $('.menu__nav li a').on('mouseleave', function() {
            TweenMax.to('.menu__nav li span', .2,{ width: 0, ease: Power2.easeInOut }, 0);
         });
    }
}
nav.init();


var home = {
    init: function() {
        var check = $('.main--home').length;

        if ( check > 0 ) {
            home.boxes();
        }
    },

    boxes: function() {
        TweenMax.to('.box--1', 1,{ opacity: 1, width: '100%', ease: Power2.easeInOut });
        TweenMax.to('.box--2', 1,{ opacity: 1, width: '100%', ease: Power2.easeInOut });
        TweenMax.to('.box--3', 1,{ opacity: 1, width: '100%', ease: Power2.easeInOut });
        TweenMax.to('.box--4', 1,{ opacity: 1, width: '50%', ease: Power2.easeInOut });
        TweenMax.to('.box--5', 1,{ opacity: 1, width: '50%', ease: Power2.easeInOut });
    }
}
home.init();


var strategicPriorities = {
    init: function() {
        var check = $('.main--strategicPriorities').length;

        if ( check > 0 ) {
            strategicPriorities.boxes();                
        }
    },

    boxes: function() {
         TweenMax.staggerTo('.boxThird', 1,{ opacity: 1, ease: Power2.easeInOut }, .25);
    }
}
strategicPriorities.init();


var letter = {
    init: function() {
        var check = $('.main--letter').length;

        if ( check > 0 ) {
            letter.introScroll();
            letter.blocksFadeIn();
            letter.productsFadeIn();
            letter.showVideo();
        }
    },

    showVideo: function() {
        var showVideoTL = new TimelineMax({paused: true});

        showVideoTL.to('.videoOverlay', .5, { display: 'block', autoAlpha: 1, ease: Power2.easeInOut }, 0)
                   .to('.videoOverlay__container', .5, { top: '50%', ease: Power2.easeInOut }, 0);

        $('.headerImg--1').on('click', function() {
            showVideoTL.play();
        });

        $('.videoClose').on('click', function() {
            showVideoTL.reverse();
        });

         $('.videoOverlay').on('click', function() {
            showVideoTL.reverse();
        });
    },

    introScroll: function() {
        var controller = new ScrollMagic.Controller({});

        var tween = new TimelineMax ()
            .add([
                TweenMax.to(".intro", 5, {y: -200, opacity: 0, ease: Linear.easeNone}),
                TweenMax .to(".header", 1, {y: -50, opacity: 0, ease: Linear.easeNone}),
                TweenMax .to(".title", 5, {y: -50, opacity: 0, ease: Linear.easeNone}),
                TweenMax .to(".headerImg--1", 5, {y: -50, opacity: 0, ease: Linear.easeNone}),
                TweenMax .to(".headerImg--2", 8, {y: -50, opacity: 0, ease: Linear.easeNone}),
                TweenMax .to(".lineElement", 10, {x: 0, ease: Linear.easeNone})
            ]);

        var scene = new ScrollMagic.Scene({triggerElement: '.intro', duration: 1000, offset: -200, triggerHook: "onLeave"})
                        .setTween(tween)
                        //.addIndicators()
                        .addTo(controller);
    },

    blocksFadeIn: function() {
        var controller = new ScrollMagic.Controller({});

         var tween = new TimelineMax ()
            .add([
                TweenMax.staggerTo(".block", 1, { opacity: 1, ease: Power2.easeInOut }, .25)
            ]);

        var scene = new ScrollMagic.Scene({triggerElement: '.blockWrap', /*duration: 1000, offset: -200,*/})
                        .setTween(tween)
                        //.addIndicators()
                        .addTo(controller);
    },

    productsFadeIn: function() {
        var controller = new ScrollMagic.Controller({});

         var tween = new TimelineMax ()
            .add([
                TweenMax.to(".productsImg", 1, { opacity: 1, ease: Power2.easeInOut }, 0)
            ]);

        var scene = new ScrollMagic.Scene({triggerElement: ".fullSection--2"})
                        .setTween(tween)
                        //.addIndicators()
                        .addTo(controller);
    }
}
letter.init();
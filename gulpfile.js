var gulp = require('gulp');
var	concat = require('gulp-concat');
var	uglify = require('gulp-uglify');
var less = require('gulp-less');
var sourcemaps = require('gulp-less-sourcemap');
var path = require('path');
var watch = require('gulp-watch');
var plumber = require('gulp-plumber');



gulp.task('build', function() {
  return gulp.src([
  		//'./_js/greensock/plugins/ScrollToPlugin.min.js',
  		'./_js/greensock/TimelineMax.min.js',
  		'./_js/greensock/TweenMax.min.js',
  		'./_js/greensock/plugins/CSSRulePlugin.min.js',
		//'./_js/pixi.min.js',
  		'./_js/main.js'
  		])
	.pipe(plumber())
    .pipe(concat('main.min.js'))
    .pipe(uglify({ mangle: false }))
    .pipe(gulp.dest('./_js'));
});

gulp.task('less', function () {
  return gulp.src('./_css/main.less')
    .pipe(less().pipe(plumber()))
	.pipe(sourcemaps({
		sourceMap: {
            sourceMapRootpath: './_css',
        }
	}))
    .pipe(gulp.dest('./_css'));
});



// Default gulp task - run "gulp" in terminal
gulp.task('default', function() {
	gulp.watch('_js/main.js', ['build']);
	gulp.watch('_css/main.less', ['less']);
});

// uglifys and concatonates js only – run "build" in terminal
gulp.task('gulp min', ['build']);

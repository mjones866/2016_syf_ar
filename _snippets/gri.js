// GRI INDICATORS FUNCTIONALITY
// Use this alongside of the gri.json file which will house all of the GRI information on each page.
// The current functionality is built so that it will grab either the directory name (I.E letter) or it will grab the interior page name (I.E people.php)
// In the gri.json file you will name each section by either it's directory name or page name. If it is a landing page (index.php) name it by the directory. Otherwise, name it by the page name.
// Currently this doesn't allow for duplicate page names (I.E to people.php files in different directories) but if there is enough need it can be edited.

// REQUIREMENTS:
	// Name your gri button '.griTab'
	// Inside of '.griTab' put an empty ul named '.griTab__items'.
	// The gri.init() function needs to be ran with the <?php echo $directory; ?> inside of it. (So that it will get the correct path name for the json file)



var gri = {
	init : function(dots) {
		$.getJSON(dots+"_js/gri.json", gri.fillData)
			.done(function(data){
				if ( gri.success === false) {
					TweenMax.to('.griTab',0,{autoAlpha:0});
				}
			});

		gri.click();
	},

	fillData : function(data) {
		
		if(href == "" || href == "index.php") {
			// if it's a landing page
			$.each(data.gri, function(page,indicators) {
				if(directory == this.page) {
					gri.success = true;
					$.each(this.indicators, function(indicator) {
						$('.griTab__items').append('<li>'+this.indicator+'</li>');
					});
				}
			});
		} else {
			// if it's an interior page
			$.each(data.gri, function(page,indicators) {
				if(href == this.page) {
					gri.success = true;

					$.each(this.indicators, function(indicator) {
						$('.griTab__items').append('<li>'+this.indicator+'</li>');
					});
				}
			});
		}
	},

	click : function() {
		var clicked = false;

		$('.griTab').on('click',function() {
			var h = $('.griTab__items').height();

			if ( clicked === false ) {
				TweenMax.to('.griTab',0.3,{height:h+40});
				clicked = true;
			} else {
				TweenMax.to('.griTab',0.3,{height:30});
				clicked = false;
			}
		});
	},

	success: false
}

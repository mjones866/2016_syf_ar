/*  ------------------------------------------
	
	-- Status Indication --
	
	Use this snippet to include status
	indication on your main menu or sub-menu.

    ------------------------------------------ */


var statusIndication = function() {
	var pathName = window.location.pathname;
	var pathArray = pathName.split('/');
	var arrayLength = pathArray.length;
	var href = pathArray[arrayLength-1];
	var directory = pathArray[arrayLength-2];
	
	// Use this for main nav status indication
	var mainMenu = function() {
		var anchor;
		var anchorArray;
		var lengthArray;
		var folder;
		$('.mainMenu a').each(function() {
			anchor = $(this).attr('href');
			anchorArray = anchor.split('/');
			lengthArray = anchorArray.length;
			folder = anchorArray[lengthArray-2];
			if (folder == directory) {
				$(this).addClass('active');
			}
		});
	}
	mainMenu();
	
	// Use this for sub nav status indication
	var subNavMenu = function() {
		var anchor;
		var anchorArray;
		var lengthArray;
		var link;
		$('.subNav a').each(function() {
			anchor = $(this).attr('href');
			anchorArray = anchor.split('/');
			lengthArray = anchorArray.length;
			link = anchorArray[lengthArray-1];
			if (link == href) {
				$(this).addClass('active');
			}
		});
	}
	subNavMenu();
}
statusIndication();
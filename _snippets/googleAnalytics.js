/*  -----------------------------------------------------------------------
	
	-- Google Analytics --
	
	Use this snippet to track pageviews and clicks throughout the site.
	
	Replace X's with the correct UA code from Google Analytics account.
	
	Add the corresponding class and data-attribute for advanced tracking.
	
	----------------------------------------------------------------------- */


// Google Analytics

(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-XXXXXXXX-X', 'auto');
ga('send', 'pageview');

// Use on PDF downloads by adding a class of 'pdfClick' and the data attribute 'pdf-id' to the link
// Example: <a class="pdfClick" pdf-id="Mohawk 2015 CRR" href="MHK_2015_CRR.pdf">Download</a>
$('.pdfClick').on('click', function() {
	var pdf = $(this).attr('pdf-id');
	ga('send', {
	  hitType: 'event',
	  eventCategory: 'PDF Download',
	  eventAction: pdf
	});
});

// Use on lightboxes by adding a class of 'lbClick' and the data attribute 'lb-id' to the link
// Example: <a class="lbClick" pdf-id="Being Sustainably Sustainable" href="#lightbox">Read more</a>
$('.lbClick').on('click', function() {
	var lightbox = $(this).attr('lb-id');
	ga('send', {
	  hitType: 'event',
	  eventCategory: 'Lightbox Open',
	  eventAction: lightbox
	});
});

// Use on external links by adding a class of 'extClick'
// Example: <a class="extClick" href="http://www.corporatereport.com/" target="_blank">Go here</a>
$('.extClick').on('click', function() {
	var hrefAttr = $(this).attr('href');
	ga('send', {
	  hitType: 'event',
	  eventCategory: 'External URL',
	  eventAction: hrefAttr
	});
});

// Add a class of 'hashClick' to links with a hash
// Example: <a class="hashClick" href="gallery.html#hash">Download</a>
$('.hashClick').on('click', function() {
	var hrefAttr = $(this).attr('href');
	ga('send', 'pageview', hrefAttr);
});

// Wrap your social sharing links in a class of 'social' and add the corresponding class to each link
// Example: <div class="social"><a class="Facebook" href="#"><i class="fa fa-facebook"></i></a></div>
$('.social a').on('click', function(e) {
	e.preventDefault();
	var network = $(this).attr('class');
	var url = '';
	var redirect = encodeURIComponent(window.location);
	switch(network) {
		case 'Facebook':
			url = 'https://www.facebook.com/sharer/sharer.php?u=' + redirect;
			break;
		case 'Twitter':
			url = 'https://twitter.com/home?status=Check%20out%20this%20link:%0A%0A' + redirect;
			break;
		case 'LinkedIn' :
			url = 'https://www.linkedin.com/shareArticle?mini=true&url='+ redirect +'&summary=&source=';
			break;
		case 'Google' :
			url = 'https://plus.google.com/share?url=' + redirect;
			break;
		default:
			break;
	}
	window.open(url);
	ga('send', {
		hitType: 'event',
		eventCategory: 'Social Share',
		eventAction: network
	});
});
<meta charset="UTF-8">
<title>Synchrony 2016 Annual Report</title>
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700" rel="stylesheet">
<link rel="stylesheet" href="<?php echo $directory; ?>_css/main.css">

<!--FOR SITES THAT REQUIRE IE8 SUPPORT USE 1.8.3 instead of 2.1.4 -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="<?php echo $directory; ?>_js/greensock/TweenMax.min.js"></script>
<script src="<?php echo $directory; ?>_js/greensock/TimelineMax.min.js"></script>
<script src="<?php echo $directory; ?>_js/greensock/plugins/CSSRulePlugin.min.js"></script>
<script src="<?php echo $directory; ?>_js/ScrollMagic.js"></script>
<script src="<?php echo $directory; ?>_js/plugins/debug.addIndicators.js"></script>
<script src="<?php echo $directory; ?>_js/plugins/animation.gsap.js"></script>

<script src="<?php echo $directory; ?>_js/main.js"></script>
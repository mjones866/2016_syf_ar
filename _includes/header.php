<header class="">
	<div class="burger">
		<?php include('_img/global/burger.svg'); ?>
	</div>
	<div class="menu">
		<div class="menu__logo">
			<?php include('_img/global/logo.svg'); ?>
			<h2><span>2016</span> Annual Report</h2>
		</div>
		<nav class="menu__nav">
			<ul>
				<li><a href="index.php">home</a><span></span></li>
				<li><a href="letter.php">Letter to Stakeholders</a><span></span></li>
				<li><a href="">Financial Highlights</a><span></span></li>
				<li><a href="strategic-priorities.php">Strategic Priorities</a><span></span></li>
				<li><a href="">Executive Leadership</a><span></span></li>
				<li><a href="">Board of Directors</a><span></span></li>
				<li><a href="">About Synchrony Financial</a><span></span></li>
			</ul>
		</nav>
	</div>
	<div class="website">
		<div class="navline"><?php include('_img/global/navLine.svg'); ?></div>
		<a href="#">www.synchronyfinancial.com</a>
	</div>
</header>

<div class="menuOverlay">
	<?php include('_img/global/navBg.svg'); ?>
</div>


<!-- Mobile PDF Download (remove if the site is mobile responsive) -->
<!--<div class="mobile">
	<div class="mobile__logo">
		<img src="<?php echo $directory; ?>_images/global/logo.svg">
	</div>
	<a class="pdfClick" pdf-id="Report Title (Mobile)" href="<?php echo $directory; ?>_pdf/FILENAME.pdf">
		<img class="mobile__cover" src="<?php echo $directory; ?>_images/global/reportCover.jpg">
	</a>
	<a class="mobile__download pdfClick" pdf-id="Report Title (Mobile)" target="_blank" href="<?php echo $directory; ?>_pdf/FILENAME.pdf">Download PDF</a>
</div>-->
<!DOCTYPE html>
<?php require("_assets/common.php"); ?>
<html lang="en">
<head>
<? $cri->includeContent(0,'topInc'); ?>
</head>
<body>
<? $cri->includeContent(0,'header'); ?>

<div class="main main--home">
    <div class="main__col">
        <div class="box box--1">
            <h1 class="box__title">
                <span>2016</span>
                <br>
                Annual Report
            </h1>
        </div>
        <div class="box box--2"></div>
    </div>
    <div class="main__col">
        <div class="box box--3"></div>
        <div class="boxSection">
            <div class="box box--4"></div>
            <div class="box box--5"></div>
        </div>
    </div>
    <div class="homeLine">
        <div class="homeLine__one"></div>
        <div class="homeLine__two"></div>
    </div>
</div>

<? $cri->includeContent(0,'botInc'); ?>
</body>
</html>
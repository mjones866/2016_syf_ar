<!DOCTYPE html>
<?php require("_assets/common.php"); ?>
<html lang="en">
<head>
<? $cri->includeContent(0,'topInc'); ?>
</head>
<body>
<? $cri->includeContent(0,'header'); ?>

<div class="main main--strategicPriorities">
    <div class="main__col">
        <div class="boxThird boxThird--1">
            <h1>Our<br>Stragetic<br>Priorities</h1>
        </div>
        <div class="boxThird boxThird--2">
            <div class="contentWrap">
                <div class="boxThird__title">
                    <h2>Grow our business through<br>our three sales platforms</h2>
                </div>
                <div class="row">
                    <div class="boxThird__content">
                        <ul>
                            <li>Accelerate capabilities: marketing, analytics and loyalty</li>
                            <li>Continue to leverage SKU level data and invest in CRM to differentiate marketing capabilities</li>
                            <li>Deliver leading capabilities across digital and mobile technologies</li>
                        </ul>
                    </div>
                    <div class="boxThird__icon boxThird__icon--magnifier">
                        <?php include('_img/strategic-priorities/icon1.svg'); ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="boxThird boxThird--3">
            <div class="contentWrap">
                <div class="boxThird__title">
                    <h2>Leverage a strong<br>capital position</h2>
                </div>
                <div class="row">
                    <div class="boxThird__content">
                        <ul>
                            <li>Organic growth, program acquisitions, and start-up opportunities</li>
                            <li>Continue capital plan execution through dividends and share repurchase program, subject to Board and regulatory approvals</li>
                            <li> Invest in capability-enhancing technologies and businesses</li>
                        </ul>
                    </div>
                    <div class="boxThird__icon boxThird__icon--arrows">
                        <?php include('_img/strategic-priorities/icon2.svg'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="main__col">
        <div class="boxThird boxThird--4">
            <div class="contentWrap">
                <div class="boxThird__title">
                    <h2>Grow our business through<br>our three sales platforms</h2>
                </div>
                <div class="row">
                    <div class="boxThird__content">
                        <ul>
                            <li>Grow existing retailer penetration </li>
                            <li>Continue to innovate and provide robust cardholder value propositions</li>
                            <li>Add new partners and programs with attractive risk and return profiles</li>
                        </ul>
                    </div>
                    <div class="boxThird__icon boxThird__icon--hands">
                        <?php include('_img/strategic-priorities/icon3.svg'); ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="boxThird boxThird--5">
            <div class="contentWrap">
                <div class="boxThird__title">
                    <h2>Position business for<br>long-term growth</h2>
                </div>
                <div class="row">
                    <div class="boxThird__content">
                        <ul>
                            <li>Explore opportunities to expand the core business (e.g., grow small business platform)</li>
                            <li>Continue to grow Synchrony Bank — enhance offerings to increase loyalty, diversify funding and drive profitability</li>
                        </ul>
                    </div>
                    <div class="boxThird__icon boxThird__icon--charts">
                        <?php include('_img/strategic-priorities/icon4.svg'); ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="boxThird boxThird--6">
           <div class="contentWrap">
                <div class="boxThird__title">
                    <h2>Operate with a strong balance<br>sheet and financial profile</h2>
                </div>
                <div class="row">
                    <div class="boxThird__content">
                        <ul>
                            <li>Maintain strong capital and liquidity</li>
                            <li>Deliver earnings growth at attractive returns</li>
                        </ul>
                    </div>
                    <div class="boxThird__icon boxThird__icon--money">
                        <?php include('_img/strategic-priorities/icon5.svg'); ?>
                    </div>
                </div>
           </div>
        </div>
    </div>
</div>

<? $cri->includeContent(0,'botInc'); ?>
</body>
</html>
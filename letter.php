<!DOCTYPE html>
<?php require("_assets/common.php"); ?>
<html lang="en">
<head>
<? $cri->includeContent(0,'topInc'); ?>
</head>
<body>
<? $cri->includeContent(0,'header'); ?>

<div class="main main--letter">
    <div class="lineHeader">
        <?php include('_img/letter/lineHeader.svg'); ?>
    </div>
    <div class="container">
        <section class="header">
            <h1>Letter to<br>Shareholders</h1>
        </section>
        <section>
            <div class="row">
                <div class="section__half">
                    <img class="headerImg headerImg--1" src="_img/letter/img1.jpg"/>
                    <img class="headerImg headerImg--2" src="_img/letter/img2.jpg"/>
                    <div class="title">
                        <h2>Margaret Keane<br><span>President and<br>Chief Executive Officer</span></h2>
                    </div>
                </div>
                <div class="section__half">
                    <div class="intro">
                        <img src="_img/letter/tempCopy1.jpg"/>
                    </div>
                </div>
            </div>
        </section>
        <section>
            <div class="row">
                <div class="content">
                    <img src="_img/letter/tempCopy2.jpg"/>
                </div>
            </div>
        </section>
    </div>
    <div class="lineElement"><?php include('_img/letter/lineElement.svg'); ?></div>
    <section class="fullSection fullSection--1">
        <div class="container">
            <section>
                <div class="row">
                    <div class="content">
                        <img src="_img/letter/tempCopy3.png"/>
                    </div>
                </div>
            </section>
            <div class="blockWrap">
                <div class="row">
                    <div class="block block--1">
                         <img src="_img/letter/blockTempCopy1.png"/>
                    </div>
                    <div class="block block--2">
                         <img src="_img/letter/blockTempCopy2.png"/>
                    </div>
                </div>
                <div class="row">
                    <div class="block block--3">
                         <img src="_img/letter/blockTempCopy3.png"/>
                    </div>
                    <div class="block block--4">
                         <img src="_img/letter/blockTempCopy4.png"/>
                    </div>  
                </div>
            </div>
            <section>
                <div class="row">
                    <div class="content">
                        <img src="_img/letter/tempCopy4.png"/>
                    </div>
                </div>
            </section>
        </div>
    </section>

    <section>
        <div class="container">
            <div class="row">
                <div class="content">
                    <img src="_img/letter/tempCopy5.jpg"/>
                </div>
            </div>
        </div>
    </section>
    <section class="fullSection fullSection--2">
        <div class="container">
            <img class="productsImg" width="100%" src="_img/letter/tempCopy6.png"/>
        </div>
    </section>
     <section>
        <div class="container">
            <div class="row">
                <div class="content">
                    <img src="_img/letter/tempCopy7.png"/>
                </div>
            </div>
        </div>
    </section>
    <section>
        <div class="container">
            <div class="row">
                <div class="content">
                    <img style="margin-left: -170px;" src="_img/letter/tempCopy8.jpg"/>
                </div>
            </div>
        </div>
    </section>
    <section>
        <div class="container">
            <div class="row">
                <div class="content">
                    <img src="_img/letter/tempCopy9.jpg"/>
                </div>
            </div>
        </div>
    </section>
</div>

<div class="videoOverlay">
    <div class="videoOverlay__container">
        <div class="videoClose"><?php include('_img/letter/close.svg'); ?></div>
        <img src="_img/letter/videoPlaceholder.jpg"/>
    </div>
</div>

<? $cri->includeContent(0,'botInc'); ?>
</body>
</html>